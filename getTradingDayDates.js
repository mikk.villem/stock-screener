require("dotenv").config();
const axios = require("axios");

const getTradingDayDates = async (startDay = 0, endDay = 10) => {
  let res = await axios
    .get(
      `https://www.alphavantage.co/query?function=TIME_SERIES_DAILY_ADJUSTED&symbol=IBM&outputsize=compact&apikey=${process.env.ALPHAVANTAGE_API_KEY}`
    )
    .then((resp) => {
      return Object.keys(resp.data["Time Series (Daily)"]).slice(
        startDay,
        endDay
      );
    })
    .catch((err) => console.error("getDate error: %s", err));

  return res;
};

module.exports = getTradingDayDates;
