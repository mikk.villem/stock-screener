//node.modules imports
const axios = require("axios");
const faunadb = require("faunadb");
const { Create, Collection, Get, Match, Index } = faunadb.query;

require("dotenv").config();

//local imports
const exceptionList = require("./exceptionList.json");
const getTradingDayDates = require("./getTradingDayDates");
const getDailyMktData = require("./getDailyMktData");

//dbConnect
const faunaClient = new faunadb.Client({
  secret: process.env.FAUNADB_SERVER_KEY,
  domain: "db.eu.fauna.com",
});

const getResultByTicker = (ticker, resultArray) => {
  return resultArray.filter((elem) => elem["T"] === ticker)[0];
};

const getTickerArrayFromResults = (arr) => {
  return arr.map((elem) => elem["T"]);
};

const removeExceptions = (inputArray, ...exceptionArrays) => {
  let filteredArray = [...inputArray];
  exceptionArrays.forEach((exceptionArray) => {
    const toRemove = new Set(exceptionArray);
    filteredArray = filteredArray.filter((x) => !toRemove.has(x));
  });

  return filteredArray;
};

const intersectDailyResults = (currentFilteredResults, prevFilteredResults) => {
  return currentFilteredResults.filter((elem) =>
    getTickerArrayFromResults(prevFilteredResults).includes(elem["T"])
  );
};

const filterResultsByIntradailyParams = (arr) => {
  return arr.filter(
    (elem) =>
      elem["T"].length < 5 &&
      elem["c"] > 15 &&
      elem["v"] * elem["c"] > 1000000 &&
      elem["c"] > elem["o"]
  );
};

const filterResultsByInterdailyParams = (
  currentDailyResultsArr,
  prevDailyResultsArr
) => {
  const currentFilteredResults = filterResultsByIntradailyParams(
    currentDailyResultsArr
  );

  const prevFilteredResults =
    filterResultsByIntradailyParams(prevDailyResultsArr);

  const intersectedTickers = intersectDailyResults(
    currentFilteredResults,
    prevFilteredResults
  );

  return getTickerArrayFromResults(intersectedTickers).filter((elem) => {
    let resultToday = getResultByTicker(elem, currentFilteredResults);
    let resultBefore = getResultByTicker(elem, prevFilteredResults);
    return (
      resultToday.v > resultBefore.v &&
      resultToday.c > resultBefore.c &&
      resultToday.c - resultToday.o > resultBefore.c - resultBefore.o
    );
  });
};

const sendResultsToDB = async (dailyResultsArray) => {
  await dailyResultsArray.forEach(({ dailyFinalResultTickers, currentDate }) =>
    faunaClient
      .query(
        Create(Collection("DailyScreens"), {
          data: {
            date: currentDate,
            resultCount: dailyFinalResultTickers.length,
            results: dailyFinalResultTickers,
          },
        })
      )
      .then((ret) => console.log(ret, "Added to DB"))
      .catch((err) => console.error("Error: %s", err))
  );

  axios
    .post(
      `https://api.netlify.com/build_hooks/${process.env.NETLIFY_BUILD_ID}?trigger_title=Screener+Deploy+${dailyResultsArray[0].currentDate}`,
      {}
    )
    .then((res) => {
      if (res.status === 200) console.log("Build Started @ Netlify");
    })
    .catch((error) => {
      console.log(error);
    });
};

const getDailyScreenData = async (startDaysFromToday = 0, daysToCover = 1) => {
  let lastXTradingDays = await getTradingDayDates(
    startDaysFromToday < 0 ? 0 : startDaysFromToday,
    startDaysFromToday + (daysToCover < 1 ? 1 : daysToCover) + 1
  );

  let results = [];

  for (i = 0; i < lastXTradingDays.length - 1; i++) {
    let currentDate = lastXTradingDays[i];
    let prevDate = lastXTradingDays[i + 1];

    let dbResult = await faunaClient
      .query(Get(Match(Index("screenByDate"), currentDate)))
      .then((res) => {
        console.log("Found in DB: %s", res.data);
        return res.data;
      })
      .catch((err) => {
        if (err.description === "Set not found.")
          console.log(`${currentDate} not in DB, proceeding...`);
        else console.error("Fauna Get Error: %s", err);
      });

    if (dbResult)
      return console.log("Skipping filtering. Skipping DB writing.");

    const { dailyResults: currentDailyResults } = await getDailyMktData(
      currentDate
    );

    const { dailyResults: prevDailyResults } = await getDailyMktData(prevDate);

    if (currentDailyResults && prevDailyResults && currentDate) {
      const dailyFinalResultTickers = removeExceptions(
        filterResultsByInterdailyParams(
          currentDailyResults.results,
          prevDailyResults.results
        ),
        exceptionList.inverseEtfs
      );
      results.push({ dailyFinalResultTickers, currentDate });
    }
  }

  sendResultsToDB(results);
};

module.exports = getDailyScreenData;
