//node.modules imports
const axios = require("axios");

require("dotenv").config();

const getDailyMktData = async (date = "2022-05-16") => {
  let dailyResults;

  dailyResults = await axios
    .get(
      `https://api.polygon.io/v2/aggs/grouped/locale/us/market/stocks/${date}?adjusted=true&apiKey=${process.env.POLYGON_API_KEY}`
    )
    .then((resp) => {
      return resp.data;
    })
    .catch((err) => console.error(`${date} dailyResults %s`, err));

  return { dailyResults };
};

module.exports = getDailyMktData;
