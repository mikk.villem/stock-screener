const response = require("./response.json");

let quoteArray = [];

for (const [key, value] of Object.entries(response["Time Series (Daily)"])) {
  let entry = { ...value };
  entry["6. date"] = key;
  quoteArray.push(entry);
}

const getQuoteMovingAverage = (timeSeriesArray, period) => {
  return (
    timeSeriesArray
      .slice(0, period)
      .map((a) => parseFloat(a["4. close"]))
      .reduce((next, number) => next + number) / period
  );
};
